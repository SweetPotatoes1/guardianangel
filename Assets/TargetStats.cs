﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetStats : CharacterStats
{
    // Start is called before the first frame update

    public override void Die()
    {
        gameObject.SetActive(false);
        for (int i = 0; i<ListManager.instance.objects.Count; i++)
        {
            if(ListManager.instance.objects.Contains(this.gameObject))
            {
                ListManager.instance.objects.Remove(this.gameObject);
            }
        }
    }
}
