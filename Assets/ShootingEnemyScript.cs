﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingEnemyScript : EnemyScript
{
    EnemyAIMovement movement;
    EnemyStats stats;
    GunScript gun;
    private void OnDisable()
    {
        this.GetComponent<SphereMovement>().enabled = false;
        this.GetComponent<EnemyCamera>().enabled = false;
        isPossessed = false;
    }
    private void OnEnable()
    {      
        transform.position = new Vector3(-10f, 16f, 10f);  
        movement = GetComponent<EnemyAIMovement>();
        movement.enabled = true;
        stats = GetComponent<EnemyStats>();
        agent.enabled = true;
        agent.stoppingDistance = 2.5f;
       
    }
    // Start is called before the first frame update
    void Start()
    {
       // stats.range = 20.0f;
    }

   
    // Update is called once per frame
    void Update()
    {
        if (isPossessed)
        {
            Renderer renderer = GetComponent<Renderer>();
            renderer.materials[0].color = Color.green;
        }
        if (Input.GetKeyDown(KeyCode.F))
        {

            GetsDepossessed();
        }
        if (isPossessed)
        {
            if (Input.GetKey(KeyCode.Mouse0))
            {
                PlayerAttack();
            }
        }
    }

    public override void Attack(GameObject Object)
    {
        base.Attack(Object);
    }
    public override void PlayerAttack()
    {
        //base.PlayerAttack();
        gun = GetComponentInChildren<GunScript>();
        if (gun != null)
        {
            gun.Shoot();
        }
        else Debug.Log("no gun script found");
       
    }
}
