﻿using UnityEngine;
public interface IPossessable
{
    void GetsPossessed();
    void GetsDepossessed();
    void MoveToPoint(Vector3 point);
    void Attack(GameObject Object);
    void PlayerAttack();

    bool ispossessed();
}
