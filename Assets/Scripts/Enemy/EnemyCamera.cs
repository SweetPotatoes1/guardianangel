﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCamera : MonoBehaviour
{


    // Start is called before the first frame update
    public float RotationSpeed = 1f;
    public Transform player, target, empty, gun, obstruction;
    private float mouseX, mouseY, zoomSpeed;
    void Start()
    {
        target = PlayerBehaviour.target;
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.Locked;
        obstruction = target;
        zoomSpeed = 3f;
        empty = target;
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void LateUpdate()
    {
        CamControl();
       // viewObstructed();
    }

    void CamControl()
    {
        mouseX += Input.GetAxis("Mouse X") * RotationSpeed;
        mouseY -= Input.GetAxis("Mouse Y") * RotationSpeed;
        mouseY = Mathf.Clamp(mouseY, -60, 30);

        //empty.transform.position = target.transform.position + new Vector3(2f, 0f, 0f);
        transform.LookAt(target);
        target.rotation = Quaternion.Euler(mouseY, mouseX, 0);
        player.rotation = Quaternion.Euler(0, mouseX, 0);
        if(gun != null)
        {
            gun.rotation = Quaternion.Euler(mouseY - 180, mouseX, 180);
            
          
           
        }

       // target.transform.Translate(-Vector3.forward * mouseY * zoomSpeed * Time.deltaTime);
        if(Input.GetKey(KeyCode.Mouse1))
        {
            target.transform.Translate(new Vector3(1.2f,-0.4f,1.2f) * 20 * zoomSpeed * Time.deltaTime);
        }
    
    }

    /*  void viewObstructed()
      {
          RaycastHit hit;
          if (Physics.Raycast(transform.position, target.position - transform.position, out hit, 2f))
          {
              if (hit.collider.gameObject.tag != "Player")
              {
                  obstruction = hit.transform;
                  obstruction.gameObject.GetComponent<MeshRenderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.ShadowsOnly;

                  if (Vector3.Distance(obstruction.position, transform.position) >= 3f && Vector3.Distance(transform.position, target.position) >= 1.5f)
                  {
                      transform.Translate(Vector3.forward * zoomSpeed * Time.deltaTime);
                  }
              }
              else
              {
                  obstruction.gameObject.GetComponent<MeshRenderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
                  if (Vector3.Distance(transform.position, target.position) < 2f)
                  {
                      transform.Translate(Vector3.back * zoomSpeed * Time.deltaTime);
                  }
              }
          }
      }
      */

  
}
