﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingEnemy : EnemyScript
{

    private Vector3 playerMovement;
    private float speed;
    private bool isDropping;
    EnemyStats stats,playerTarget;
    TargetStats targetStats;
    public EnemyAIMovement movement;
    public List<GameObject> obj;
    private float dropTimer = 1f;
    
    PlayerBehaviour player;
    // Start is called before the first frame update
    void Start()
    {
       
        
    }

    private void OnDisable()
    {
        this.GetComponent<SphereMovement>().enabled = false;
        this.GetComponent<EnemyCamera>().enabled = false;
        isPossessed = false;
    }
    private void OnEnable()
    {
        transform.position = new Vector3(-10f, 16f, 10f);
        speed = 0f;
        isDropping = false;
        agent.stoppingDistance = 0f;
        movement = GetComponent<EnemyAIMovement>();
        movement.enabled = true;
        stats = GetComponent<EnemyStats>();
        agent.enabled = true;
        dropTimer = 0.8f;
    }
    private void Update()
    {  
       // objectToAttack = compareDistance(ListManager.instance.objects);
       if (isDropping)
        {
            dropTimer -= Time.deltaTime;
            playerMovement = new Vector3(0f, speed, 0f) * 2 * Time.deltaTime;
            transform.Translate(playerMovement, Space.Self);

            if (dropTimer <= 0)
            {
                speed = -12;
            }
            else
            {
                GetComponent<EnemyCamera>().enabled = false;
                transform.Rotate(new Vector3(0f, 500f, 0f) * Time.deltaTime);
            }
        }
       
     

      /*  if(movement != null)
        {
            obj = ListManager.instance.objects;
            if (obj.Contains(this.gameObject))
            {
                obj.Remove(this.gameObject);
            }
          
          
            movement.objectToAttack = movement.compareDistance(obj);
            if (movement.objectToAttack != null)
            {
               // Debug.Log(movement.objectToAttack);
            }
            if (((transform.position.z - movement.objectToAttack.transform.position.z) == 0f)
           &
          ((transform.position.x - movement.objectToAttack.transform.position.x) == 0f))
            {
                if(!isPossessed)
                {
                   //  Debug.Log("@@#!@#!@#");
                    Attack(movement.objectToAttack);
                }
               

            }
        }
       */

        if (Input.GetKeyDown(KeyCode.F))
        {

            GetsDepossessed();
        }

        if (isPossessed)
        {
            if(Input.GetKeyDown(KeyCode.Mouse0))
            {
                PlayerAttack();
            }
        }
        
    }
    public override void PlayerAttack()
    {
        //base.PlayerAttack();
        Attack(movement.objectToAttack);
       
    }

    public override void Attack(GameObject Object)
    {
        //base.Attack(Object);
        dropTimer = 0.8f;
        GetComponent<EnemyAIMovement>().enabled = false;
        speed = 1f;
        isDropping = true;
        agent.enabled = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        player = other.GetComponent<PlayerBehaviour>();
        Target target = other.GetComponent<Target>();
        if (target != null)
        {
            targetStats = other.GetComponent<TargetStats>();
            if (targetStats != null)
            {
                targetStats.TakeDamage(4000);
            }
            
        }
        IPossessable possessable = other.GetComponent<IPossessable>();
        if (possessable != null)
        {
            if (possessable.ispossessed())
            {
                stats = other.GetComponent<EnemyStats>();
                if (stats!= null)
                {
                    stats.TakeDamage(100);
                }
                
            }
        }
        if (player == null)
        {
            myStats.Die();
            target = null;
            possessable = null;
        }
        /*  if (possessable != null)
          {
              if (possessable.ispossessed())
              {
                  myStats.Die();
                //  gameObject.SetActive(false);
              }
          }
          if (target != null)
          {
              myStats.Die();
             // gameObject.SetActive(false);
          }
          */

    }
    
}
