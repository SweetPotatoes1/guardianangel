﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;


public class EnemyAIMovement : MonoBehaviour
{
   
    private bool hasAttacked = false;
    private float timer;
    private float lookRadius = 25.0f;
    
    EnemyScript enemyscript;
    CharacterStats stats;
    public GameObject objectToAttack;
    private bool validTarget = false;
    [SerializeField]
    private Vector3 destination;
    [SerializeField]
    public EnemyState _currentState;

    // Start is called before the first frame update
    void Start()
    {
        stats = GetComponent<CharacterStats>();
        timer = (1 / stats.attackSpeed);
        enemyscript = GetComponent<EnemyScript>();
        Renderer renderer = GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {

        switch (_currentState)
        {
            case EnemyState.wander:
                {
                    Renderer renderer = GetComponent<Renderer>();
                    renderer.materials[0].color = Color.cyan;
                    if (destination == Vector3.zero)
                    {
                        GetDestination();
                    }
                    if (NeedsDestination())
                    {
                        GetDestination();
                    }
                    enemyscript.MoveToPoint(destination);
                    objectToAttack = compareDistance(ListManager.instance.objects);
                    if(objectToAttack != null)
                    {
                      //  if(Vector3.Distance(transform.position, objectToAttack.transform.position) <= lookRadius)
                      if((Mathf.Abs(transform.position.x - objectToAttack.transform.position.x) <= lookRadius) && (Mathf.Abs(transform.position.z - objectToAttack.transform.position.z) <= lookRadius))
                        {
                            validTarget = true;
                            _currentState = EnemyState.chase;
                        }
                       
                       
                    }
                    break;
                }
            case EnemyState.chase:
                {
                    Renderer renderer = GetComponent<Renderer>();
                    renderer.materials[0].color = Color.black;
                    IPossessable possessable = objectToAttack.GetComponent<IPossessable>();
                    if (possessable != null)
                    {
                        if (!possessable.ispossessed())
                        {
                            validTarget = false;
                        }
                    }
                    if(!validTarget)
                    {
                        _currentState = EnemyState.wander;
                        return;
                    }
                    //  if (Vector3.Distance(transform.position, target.transform.position) > lookRadius)
                    if ((Mathf.Abs(transform.position.x - objectToAttack.transform.position.x) <= lookRadius) && (Mathf.Abs(transform.position.z - objectToAttack.transform.position.z) > lookRadius))
                    {
                        _currentState = EnemyState.wander;
                        //target = null;
                    }
                    enemyscript.MoveToPoint(objectToAttack.transform.position);
                    //  if (Vector3.Distance(target.transform.position, transform.position) <= enemyscript.agent.stoppingDistance)
                    if ((Mathf.Abs(transform.position.x - objectToAttack.transform.position.x) <=  stats.range) // enemyscript.agent.stoppingDistance)
                        &&
                       (Mathf.Abs(transform.position.z - objectToAttack.transform.position.z) <= stats.range)) // enemyscript.agent.stoppingDistance))
                    {
                        _currentState = EnemyState.attack;
                    }
                    break;
                }
            case EnemyState.attack:
                {
                    Renderer renderer = GetComponent<Renderer>();
                    renderer.materials[0].color = Color.red;
                    if (!objectToAttack.activeInHierarchy)
                    {
                        validTarget = false;
                    }
                    IPossessable possessable = objectToAttack.GetComponent<IPossessable>();
                    if (possessable != null)
                    {
                        if (!possessable.ispossessed())
                        {
                            validTarget = false;
                        }
                    }
                   
                    if (!validTarget)
                    {
                        _currentState = EnemyState.wander;
                    }
                    else
                    {
                        // if(Vector3.Distance(target.transform.position, transform.position) > enemyscript.agent.stoppingDistance && target !=null)
                        if ((Mathf.Abs(transform.position.x - objectToAttack.transform.position.x) > stats.range)
                            &&
                           (Mathf.Abs(transform.position.z - objectToAttack.transform.position.z) > stats.range)
                           && validTarget)
                        {
                            _currentState = EnemyState.chase;
                        }
                        else
                        {
                            FaceTarget(objectToAttack);
                            enemyscript.MoveToPoint(this.transform.position);
                            if (!hasAttacked)
                            {
                                enemyscript.Attack(objectToAttack);
                                hasAttacked = true;
                            }
                        }
                        
                    }
                    break;

                   
                }
        }

        if(hasAttacked)
        {
            timer -= Time.deltaTime;
            if(timer <= 0)
            {
                hasAttacked = false;
                timer = 1/stats.attackSpeed;
            }
        }
        //Out of all targets, choose the closest one
     

       /* Move to the closest target
        IF IN RANGE OR IF ANY AVAILABLE
        enemyscript.MoveToPoint(objectToAttack.transform.position);

        If in range, start attacking the closest target
        if(Vector3.Distance(objectToAttack.transform.position, transform.position) <= enemyscript.agent.stoppingDistance)
        {
            FaceTarget(objectToAttack);
            if (!hasAttacked)
            {
                enemyscript.Attack(objectToAttack);
                hasAttacked = true;
            }
            
        }
        */
       
    }
    // returns the closest gameobject to this gameobject from a list 
    public GameObject compareDistance(List<GameObject> x)
    {
        float minDistance = 999999999999999999999999f;
        int index = 0;
        for (int i = 0; i<x.Count; i++)
        {
            if (minDistance > Vector3.Distance(x[i].transform.position, transform.position))
            {
                minDistance = Vector3.Distance(x[i].transform.position, transform.position);
                index = i;
            }

        }
        if(x.Count == 0)
        {
            return null;
        }
        else
        {
            return (x[index]);
        }
            
                  
    }

    void FaceTarget(GameObject obj)
    {
        Vector3 Direction = (obj.transform.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(Direction.x, 0, Direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5);
    }

    private bool NeedsDestination()
    {
     //   if (Vector3.Distance(transform.position, destination) <= enemyscript.agent.stoppingDistance)
     if(Mathf.Abs(transform.position.x - destination.x) <= enemyscript.agent.stoppingDistance && Mathf.Abs(transform.position.z - destination.z) <= enemyscript.agent.stoppingDistance)
        {
            return true;
        }
        else return false;
    }

    private void GetDestination()
    {
        Vector3 test = (transform.position + (transform.forward * 3f)) +
                           new Vector3(Random.Range(-5f, 5f), 1 , Random.Range(-5f, 5f));

        destination = test;
    }

    public enum EnemyState
    {
        wander,
        chase,
        attack,
    }
}

