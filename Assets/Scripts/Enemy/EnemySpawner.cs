﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemySpawner : MonoBehaviour
{
    public bool canSpawn;
    //public GameObject[] enemiesToSpawn;
    public Vector3 SpawnValues, SpawnFlyingValues, offset;
    public float spawnrate;
    public bool spawnsFlying, spawnsShooting, spawnsWalking;
    EnemyScript enemyScript;

    // Start is called before the first frame update
    void Start()
    {
        canSpawn = true;
        StartCoroutine(waitSpawner());
    }

    // Update is called once per frame
    void Update()
    {

    }


    IEnumerator waitSpawner()

    {
        yield return new WaitForSeconds(1.0f);
        while (canSpawn)
        {
            if (spawnsShooting)
            {
                Vector3 spawnPosition = new Vector3(Random.Range(SpawnValues.x - offset.x, SpawnValues.x + offset.x), 1, Random.Range(SpawnValues.z - offset.z, SpawnValues.z + offset.z));
                GameObject enemy = ObjectPooler.SharedInstance.getPooledObject(ObjectPooler.SharedInstance.pooledObjects0);
                if (enemy != null)
                {
                    enemy.SetActive(true);
                    enemyScript = enemy.GetComponent<EnemyScript>();
                    if(enemyScript != null)
                    {
                        enemyScript.agent.Warp(spawnPosition);
                    }
                   // enemy.transform.position = spawnPosition;
                   // Debug.Log(spawnPosition);
                    enemy.transform.rotation = this.transform.rotation;
                }
                else
                {
                    GameObject obj = Instantiate(ObjectPooler.SharedInstance.objectToPool[0]) as GameObject;
                    obj.SetActive(true);
                    ObjectPooler.SharedInstance.pooledObjects0.Add(obj);
                    ListManager.instance.enemies.Add(obj);
                }
            }
            if (spawnsFlying)
            {
                Vector3 spawnPosition = new Vector3(Random.Range(SpawnFlyingValues.x - offset.x, SpawnFlyingValues.x + offset.x), SpawnFlyingValues.y, Random.Range(SpawnFlyingValues.z - offset.z, SpawnFlyingValues.z + offset.z));
                GameObject enemy = ObjectPooler.SharedInstance.getPooledObject(ObjectPooler.SharedInstance.pooledObjects1);
                if (enemy != null)
                { 
                    enemy.SetActive(true);
                    enemyScript = enemy.GetComponent<EnemyScript>();
                    if (enemyScript != null)
                    {
                        enemyScript.agent.Warp(spawnPosition);
                    }
                    //enemy.transform.position = spawnPosition;
                    enemy.transform.rotation = this.transform.rotation;
                }
                else
                {
                    GameObject obj = Instantiate(ObjectPooler.SharedInstance.objectToPool[1]) as GameObject;
                    obj.SetActive(true);
                    obj.transform.position = spawnPosition;
                    obj.transform.rotation = this.transform.rotation;
                    ObjectPooler.SharedInstance.pooledObjects1.Add(obj);
                    ListManager.instance.enemies.Add(obj);
                }
            }
            if(spawnsWalking)
            {
                Vector3 spawnPosition = new Vector3(Random.Range(SpawnValues.x - offset.x, SpawnValues.x + offset.x), SpawnValues.y, Random.Range(SpawnValues.z - offset.z, SpawnValues.z + offset.z));
                GameObject enemy = ObjectPooler.SharedInstance.getPooledObject(ObjectPooler.SharedInstance.pooledObjects2);
                if (enemy != null)
                {
                    enemy.SetActive(true);
                    enemyScript = enemy.GetComponent<EnemyScript>();
                    if (enemyScript != null)
                    {
                        enemyScript.agent.Warp(spawnPosition);
                    }
                    //enemy.transform.position = spawnPosition;
                    enemy.transform.rotation = this.transform.rotation;
                }
                else
                {
                    GameObject obj = Instantiate(ObjectPooler.SharedInstance.objectToPool[2]) as GameObject;
                    obj.SetActive(true);
                    obj.transform.position = spawnPosition;
                    obj.transform.rotation = this.transform.rotation;
                    ObjectPooler.SharedInstance.pooledObjects1.Add(obj);
                    ListManager.instance.enemies.Add(obj);
                }
            }

           
            yield return new WaitForSeconds(spawnrate);
        }
    }
}