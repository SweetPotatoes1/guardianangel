﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class EnemyScript : MonoBehaviour, IPossessable
{

    public bool isPossessed = false;
    public NavMeshAgent agent;
    private RaycastHit hitinfo;
    public EnemyStats myStats, targetstats;
    ColliderScript ColliderScript;
    
   
    public static EnemyScript instance;
  

    private void OnEnable()
    {
        myStats = this.GetComponent<EnemyStats>();
        agent = GetComponent<NavMeshAgent>();
        agent.speed = 5f;
        agent.stoppingDistance = 2.5f;
        this.GetComponent<SphereMovement>().enabled = false;
        this.GetComponent<EnemyAIMovement>().enabled = true;
        this.GetComponent<EnemyCamera>().enabled = false;
        this.GetComponent<NavMeshAgent>().enabled = true;
        ColliderScript = GetComponentInChildren<ColliderScript>();
        this.GetComponentInChildren<SphereCollider>().enabled = false;
    }

    private void OnDisable()
    {
        //GetsDepossessed();
        this.GetComponent<SphereMovement>().enabled = false;
        this.GetComponent<EnemyAIMovement>().enabled = true;
        this.GetComponent<NavMeshAgent>().enabled = true;
        this.GetComponent<EnemyCamera>().enabled = false;
        isPossessed = false;


    }
    private void Awake()
    {
      
        instance = this;
      

    }
    void Start()
    {

        //AttackSpeed = 2.0f;
        
        
      

    }

    // Update is called once per frame
    void Update()
    {
        if (isPossessed)
        {
            Renderer renderer = GetComponent<Renderer>();
            renderer.materials[0].color = Color.magenta;
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                PlayerAttack();
                Debug.Log(PlayerBehaviour.possessed + "is attacking");
                if (ColliderScript != null)
                {


                    if (ColliderScript.ObjectToAttack != null)
                    {
                        targetstats = ColliderScript.ObjectToAttack.GetComponent<EnemyStats>();
                    }
                    else
                    {
                        return;
                    }
                }
                else
                {
                    return;
                }

                if (targetstats != null)
                {
                    targetstats.TakeDamage(myStats.damage.GetValue());
                    Debug.Log("player dealing dmg");
                    ColliderScript.ObjectToAttack = null;
                }
            }
        }
        if (Input.GetKeyDown(KeyCode.F))
        {
           
            GetsDepossessed();
        }

    }

    public virtual void PlayerAttack()
    {
        ColliderScript.self.enabled = true;
    }

    /* void FixedUpdate()
     {
         int layermask = 1 << 8;
         Vector3 fwd = transform.TransformDirection(Vector3.forward);

         if (Physics.Raycast(transform.position, fwd, out hitinfo, stats.range, layermask))
         {
             print("There is something in front of the object!");
         }

     }    */

    public void GetsPossessed()
    {
        
        this.GetComponent<SphereMovement>().enabled = true;
        this.GetComponent<EnemyCamera>().enabled = true;
        this.GetComponent<EnemyAIMovement>().enabled = false;
        this.GetComponent<NavMeshAgent>().enabled = false;
        isPossessed = true;
        myStats.attackSpeed = 3f;
    



    }

    public void GetsDepossessed()
    {
        //dereference gameobject
        this.GetComponent<SphereMovement>().enabled = false;
        this.GetComponent<EnemyAIMovement>().enabled = true;
        this.GetComponent<NavMeshAgent>().enabled = true;
        this.GetComponent<EnemyCamera>().enabled = false;
   
        PlayerBehaviour.possessed = null;
        PlayerBehaviour.isPossessing = false;
        isPossessed = false;
        myStats.attackSpeed = 0.5f;
        // movement.objects.Remove(this.gameObject);
        // movement.enemies.Add(this.gameObject);
      /*  for (int i = 0; i < movement.objects.Count; i++)
        {
            othermovement = movement.objects[i].GetComponent<EnemyAIMovement>();
            if (othermovement.gameObject != this.gameObject)
            {
                othermovement.objects.Remove(this.gameObject);
                othermovement.enemies.Add(this.gameObject);
            }

        }
        */
    }

    public void MoveToPoint(Vector3 point)
    {
        agent.SetDestination(point);
        
    }

    public virtual void Attack(GameObject Object)
    { 
        CharacterStats stats = Object.GetComponent<CharacterStats>();
        stats.TakeDamage(myStats.damage.GetValue());
        Debug.Log("dealtDMG");
    }

 public bool ispossessed()
    {
        return isPossessed;
    }
}

