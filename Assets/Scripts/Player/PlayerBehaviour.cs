﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehaviour : MonoBehaviour
{
    Vector3 pos;
    public static bool isPossessing = false;
    private bool stoppedPos = false;
    public static GameObject possessed;
    public static Transform target;
    pula pula;
    public static PlayerBehaviour instance;
    SphereMovement movement;
   
    // Start is called before the first frame update
    void Start()
    {
        pula = GetComponentInChildren<pula>();
        target = pula.transform;
        movement = GetComponent<SphereMovement>();
    }

    private void Awake()
    {
        instance = this;
    }
    // Update is called once per frame
    void Update()
    {


        if (isPossessing)
        {
            // Attach to the object you're possessing
            pos = possessed.transform.position + new Vector3 (0.0f, 4.0f,0.0f);
            this.transform.position = pos;
            target.position = possessed.transform.position + new Vector3(0f,2f,0f);
            
        }
        else
        {
            if (!stoppedPos)
            {
                StopPossession();
                stoppedPos = true;
            }
           
        }

       
    }

    public void StopPossession()
    {

        
        this.GetComponent<MeshRenderer>().enabled = true;
        this.GetComponent<CapsuleCollider>().enabled = true;
        this.GetComponent<SphereMovement>().enabled = true;
        target.position = this.transform.position;
        isPossessing = false;
    }


    private void OnTriggerEnter(Collider col)
    {
       // Debug.Log("asdasd");
        IPossessable possessable = col.GetComponent<IPossessable>();
        if (possessable!= null)
        {
            Debug.Log("posessing " + col.gameObject.name);
            possessable.GetsPossessed();
            stoppedPos = false;
            // turn invisible and uninteractable
            this.GetComponent<MeshRenderer>().enabled = false;
            this.GetComponent<CapsuleCollider>().enabled = false;
            this.GetComponent<SphereMovement>().enabled = false;
          //  movement.playerMovement = new Vector3(0f, 0f, 0f);
            isPossessing = true;
            possessed = col.gameObject;
          //  possessed.transform.rotation = this.transform.rotation;
            
        }
    }
}
