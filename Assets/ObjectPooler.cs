﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour
{
    public static ObjectPooler SharedInstance;

    public List<GameObject> pooledObjects0, pooledObjects1, pooledObjects2;
    public GameObject[] objectToPool;
    public int amountToPool;

    private void Start()
    {
        pooledObjects0 = new List<GameObject>();
        for (int i = 0; i < amountToPool; i++)
        {
            GameObject obj = Instantiate(objectToPool[0]) as GameObject;
            obj.SetActive(false);
            pooledObjects0.Add(obj);
        }

        pooledObjects1 = new List<GameObject>();
        for(int i = 0; i < amountToPool; i++)
        {
            GameObject obj = Instantiate(objectToPool[1]) as GameObject;
            obj.SetActive(false);
            pooledObjects1.Add(obj);
        }
        pooledObjects2 = new List<GameObject>();
        for (int i = 0; i < amountToPool; i++)
        {
            GameObject obj = Instantiate(objectToPool[2]) as GameObject;
            obj.SetActive(false);
            pooledObjects2.Add(obj);
        }
    }

    private void Awake()
    {
        SharedInstance = this;
    }

    public GameObject getPooledObject(List<GameObject> ObjList)
    {
        for (int i = 0; i < ObjList.Count; i++)
        {
            if (!ObjList[i].activeInHierarchy)
            {
                return ObjList[i];
            }

        }
        return null;
    }
}