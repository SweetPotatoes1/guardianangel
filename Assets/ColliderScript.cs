﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderScript : MonoBehaviour
{

    public GameObject ObjectToAttack;
    EnemyStats stats;
    private float timer;
    
    public SphereCollider self;
    // Start is called before the first frame update
    private void OnTriggerEnter(Collider other)
    {
        IPossessable possessable = other.GetComponent<IPossessable>();
        if (possessable != null)
        {
            ObjectToAttack = other.gameObject;
        }
    }

    private void Awake()
    {
        stats = GetComponentInParent<EnemyStats>();
        timer = 1 / stats.attackSpeed;
        self = GetComponent<SphereCollider>();

        self.enabled = false;
    }
    private void Update()
    {
       if (self.enabled)
        {
            timer -= Time.deltaTime;
            if(timer <= 0)
            {
                self.enabled = false;
                ObjectToAttack = null;
                timer = 1/stats.attackSpeed;
            }
        }
    }

   public void setactive()
    {
        self.enabled = true;
    }
}
