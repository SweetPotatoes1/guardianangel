﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunScript : MonoBehaviour
{
    public int bulletsRemaining, totalBullets, bulletsPerMagazine;
    public GameObject shootPoint, gun;
    public float fireRate;
    float fireTimer = 0f;
    float range = 200f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (fireTimer < fireRate)
        {
            fireTimer += Time.deltaTime;
        }
        shootPoint.transform.rotation = gun.transform.rotation;
      
    }

    public void Shoot()
    {
     
        if (fireTimer > fireRate)
        {
           
            RaycastHit hit;

            if(Physics.Raycast(shootPoint.transform.position, -this.transform.forward , out hit, range))
            {
                Debug.Log(hit.transform.name + "hit");
                Debug.DrawLine(shootPoint.transform.position, hit.point);
                IPossessable possessable = hit.transform.gameObject.GetComponent<IPossessable>();
                if (possessable != null)
                {
                    CharacterStats enemystats = hit.transform.gameObject.GetComponent<EnemyStats>();
                    if (enemystats != null)
                    {
                        enemystats.TakeDamage(50);
                    }
                }
            }
            fireTimer = 0.0f;
        }

        else return;


    }
}
