﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterStats: MonoBehaviour
{
    public int maxHealth = 100;
    public int CurrentHealth { get; private set; }
    public stat damage;
    public stat armor;
    public float attackSpeed;
    public float range;

    private void OnEnable()
    {
        CurrentHealth = maxHealth;
    }
    private void Awake()
    {
      //  CurrentHealth = maxHealth;
    }

    private void Update()
    {
     
    }
    public void TakeDamage(int damage)
    {
        damage -= armor.GetValue();
        damage = Mathf.Clamp(damage, 0, int.MaxValue);
        CurrentHealth -= damage;
        //Debug.Log(transform.name + "takes" + damage + " damage");

        if (CurrentHealth <= 0)
        {
            Die();
            attackSpeed = 1f;
        }
    }

    public virtual void Die()
    {
        //meant to be overwritten
       // Debug.Log(transform.name + " died.");
       // Debug.Log(PlayerBehaviour.possessed);
        if(PlayerBehaviour.isPossessing)
        {
            if (PlayerBehaviour.possessed == gameObject)
            {
                PlayerBehaviour.instance.StopPossession();
                EnemyScript.instance.GetsDepossessed();
            }
            
        }
        gameObject.SetActive(false);
        

    }
}

