﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ListManager : MonoBehaviour
{
    public Target[] targets;
    public List<GameObject> objects, enemies;
    GameObject[] possessables;
    public static ListManager instance;
    // Start is called before the first frame update

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
       
    }
    void Start()
    {
        enemies = Find<IPossessable>();
        
        targets = FindObjectsOfType<Target>();
        objects = new List<GameObject>();

        for (int i = 0; i < targets.Length; i++)
        {
            //objects[i] = targets[i].gameObject;
            objects.Add(targets[i].gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {
        // If you possess an enemy, all other enemies find you as a possible target
        for (int i = 0; i < enemies.Count; i++)
        {
            IPossessable possessable = enemies[i].GetComponent<IPossessable>();
            if (possessable != null)
            {
                if (possessable.ispossessed())
                {
                    objects.Add(enemies[i]);
                    enemies.Remove(enemies[i]);
                }
            }
            

        }

        // If you stop possessing that enemy, you are no longer a target
        for (int i = 0; i < objects.Count; i++)
        {
            IPossessable possessable = objects[i].GetComponent<IPossessable>();
            if (possessable != null)
            {
                // Debug.Log(objects[i].name); 
                if ((!possessable.ispossessed()) | (!(objects[i].activeInHierarchy)))
                {
                    Debug.Log(objects[i].name + "stopped being possessed");
                    enemies.Add(objects[i]);
                    objects.Remove(objects[i]);

                }
            }
        }
    }

    [SerializeField]
    List<GameObject> Find<IPossessable>()
    {
        List<GameObject> PossessableObj = new List<GameObject>();
        GameObject[] rootGameObjects = SceneManager.GetActiveScene().GetRootGameObjects();
        foreach (var rootGameObject in rootGameObjects)
        {
            IPossessable childrenInterface = rootGameObject.GetComponentInChildren<IPossessable>();
            if (childrenInterface != null)
            {
                PossessableObj.Add(rootGameObject);
            }
        }
        return PossessableObj;
    }
}
